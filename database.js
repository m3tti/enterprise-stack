const { append, pipe, toPairs, sort, gt, toString, assoc, head, __, forEach, length, map, concat, tail, filter, reduce, not, equals, isNil, contains, curry } = require("ramda");
const logger = require("winston");
const anyDB = require("any-db");
const crypto = require('crypto');
const conn = anyDB.createConnection('sqlite3://my.db');

const sha256 = data => crypto.createHash('sha256').update(data).digest('hex');

const execute = curry((obj, callback) => {
    conn.query(obj.sql, obj.data || null , (err, res) => {
        if (err) return callback(err, null);

        return callback(err, res);
    });
});

const batch = curry((fns, acc, cb) => {
    let fn = head(fns);

    if (isNil(fn)) return cb(acc);

    return fn((err, res) => batch(tail(fns),
                                 concat(acc, [{ err: err, res: res }]),
                                 cb));
});

const batchImport = batch(__, [], __);

const initialize = () => {
    let statements = [execute({ sql: "CREATE TABLE generation (timestamp LONG, data_composition BLOB)" }),
                      execute({ sql: "CREATE TABLE data (timestamp LONG, hash VARCHAR(512), payload BLOB)" })
                     ];

    return batchImport(statements);
};
                      
const hash = pipe(toString, sha256);
const timestamp = new Date();

const dataInsert = "INSERT INTO data (timestamp, hash, payload) VALUES (?,?,?);";
const generationInsert = "INSERT INTO generation (timestamp, data_composition) VALUES (?,?);";
const metaData = data => [timestamp, hash(data), data];
const dataInsertQuery = pipe(toString,
                             metaData,
                             assoc("data",__, { sql: dataInsert }),
                             execute);

const save = curry((data, cb) => {
    let imports = pipe(map(dataInsertQuery, data);
    
    console.log(imports);
    
    batchImport(imports, cb);
});

const read = execute({ sql: "SELECT * FROM data;"});

module.exports = {
    initialize: initialize,
    save: save,
    read: read
};
