const { map, filter, reduce, not, equals, isNil, contains, curry } = require("ramda");
const logger = require("winston");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const session = require("express-session");
const express = require("express");
const db = require("./database");

const app = express();

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(helmet());
app.use(session({
        secret : 'damnAwesome',
        name : 'dafaq',
        cookie: { maxAge: 60000 }
}));

app.disable('x-powered-by');

logger.level="verbose";

app.get("/", (req, res) => {
        res.send(req.session);
});

app.get("/pug", (req, res) => {
        res.render("example", { gid: req.session.gid });
});

app.get("/signup", (req, res) => {
        req.session.gid = Math.floor(Math.random()*1000);
        res.send("DONE!");
});

db.initialize()(data => {
    logger.info(JSON.stringify(data));

    db.save([{ hello: "world" }], (err, res) => console.log(err, res));
    db.read((err, res) => console.log(err, res));
    
    app.listen(3000, () => logger.info("Running on port 3000"));
});
